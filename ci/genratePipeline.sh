#!/bin/bash

CRON=$1
BRANCH=$2
FOR=$3
COMPILE=$4
TEST=$5
BUILD=$6
URL=$7
cat ci/jenkinsfile.sample | sed 's@$cron@'"${CRON}"'@' | sed 's@$branch@'"${BRANCH}"'@' | sed 's@$compile@'"${COMPILE}"'@' | sed 's@$test@'"${TEST}"'@' | sed 's@$build@'"${BUILD}"'@' | sed 's@$repo@'"${URL}"'@' > ci/jenkinsfile
